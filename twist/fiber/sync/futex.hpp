#pragma once

// https://en.wikipedia.org/wiki/Futex

namespace twist {
namespace fiber {

// Returns 0 if the caller was woken up, -1 otherwise (EAGAIN)
int FutexWait(unsigned int* addr, int expected);

// Returns the number of waiters that were woken up
int FutexWake(unsigned int* addr, int count);

}  // namespace fiber
}  // namespace twist
