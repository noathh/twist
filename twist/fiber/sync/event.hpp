#pragma once

#include <twist/fiber/core/wait_queue.hpp>

namespace twist {
namespace fiber {

class Event {
 public:
  void Await() {
    if (!ready_) {
      wait_queue_.Park();
    }
  }

  void Signal() {
    ready_ = true;
    if (!wait_queue_.IsEmpty()) {
      wait_queue_.WakeAll();
    }
  }

 private:
  bool ready_{false};
  WaitQueue wait_queue_;
};

}  // namespace fiber
}  // namespace twist
