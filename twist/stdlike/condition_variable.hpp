#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/condvar.hpp>

namespace twist {
using condition_variable = fault::FaultyConditionVariable;  // NOLINT
}  // namespace twist

#else

#include <condition_variable>

namespace twist {
using std::condition_variable;
}  // namespace twist

#endif
