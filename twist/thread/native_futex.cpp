#include <twist/thread/native_futex.hpp>

#if LINUX

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

namespace {

// There is no glibc wrapper for 'futex' syscall

int futex(unsigned int* uaddr, int op, int val, const struct timespec* timeout,
          int* uaddr2, int val3) {
  return syscall(SYS_futex, uaddr, op, val, timeout, uaddr2, val3);
}

}  // namespace

namespace twist {
namespace thread {

int FutexWait(unsigned int* addr, int expected) {
  return futex(addr, FUTEX_WAIT_PRIVATE, expected, nullptr, nullptr, 0);
}

int FutexWake(unsigned int* addr, int count) {
  return futex(addr, FUTEX_WAKE_PRIVATE, count, nullptr, nullptr, 0);
}

}  // namespace thread
}  // namespace twist

#endif
