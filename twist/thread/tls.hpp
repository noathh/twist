#pragma once

#include <cstddef>

// Thread-local storage

namespace twist {
namespace thread {
namespace tls {

using Key = size_t;

Key AcquireKey();
void ReleaseKey(Key key);
void* AccessSlot(Key key);

}  // namespace tls
}  // namespace thread
}  // namespace twist
