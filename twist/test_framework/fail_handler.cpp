#include <twist/test_framework/fail_handler.hpp>

#include <twist/support/panic.hpp>

static ITestFailHandlerPtr handler = nullptr;

void InstallTestFailHandler(ITestFailHandlerPtr h) {
  handler = h;
}

ITestFailHandlerPtr GetTestFailHandler() {
  if (!handler) {
    PANIC("Test fail handler not installed");
  }
  return handler;
}
