#include <twist/threading/tls.hpp>

#if defined(TWIST_FIBER)
#include <twist/fiber/core/fls.hpp>
#else
#include <twist/thread/tls.hpp>
#endif

namespace twist {
namespace th {

#if defined(TWIST_FIBER)

// Fibers

Key AcquireKey() {
  return fiber::fls::AcquireKey();
}

void ReleaseKey(Key key) {
  fiber::fls::ReleaseKey(key);
}

void* AccessSlot(Key key) {
  return fiber::fls::AccessSlot(key);
}

#else

// Threads

Key AcquireKey() {
  return thread::tls::AcquireKey();
}

void ReleaseKey(Key key) {
  thread::tls::ReleaseKey(key);
}

void* AccessSlot(Key key) {
  return thread::tls::AccessSlot(key);
}

#endif

}  // namespace th
}  // namespace twist
