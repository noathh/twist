#include <twist/support/one_shot_event.hpp>

#include <twist/support/sleep.hpp>
#include <twist/test_framework/test_framework.hpp>

#include <chrono>
#include <thread>

TEST_SUITE(OneShotEvent) {
  SIMPLE_TEST(SetAndAwait) {
    twist::OneShotEvent event;

    event.Set();
    event.Await();
  }

  SIMPLE_TEST(RepeatedCalls) {
    twist::OneShotEvent event;

    event.Set();
    event.Await();

    event.Set();
    event.Await();
  }

  SIMPLE_TEST(BlockingAwait) {
    twist::OneShotEvent event;

    std::thread delayed_set([&]() {
      twist::SleepMillis(500);
      event.Set();
    });

    event.Await();

    delayed_set.join();
  }

  SIMPLE_TEST(TimedWait) {
    twist::OneShotEvent event;

    ASSERT_FALSE(event.TimedWait(std::chrono::seconds(1)));
    event.Set();
    ASSERT_TRUE(event.TimedWait(std::chrono::seconds(1)));
  }
}
