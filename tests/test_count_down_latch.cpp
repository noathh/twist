#include <twist/test_utils/count_down_latch.hpp>
#include <twist/support/sleep.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <thread>

TEST_SUITE(CountDownLatch) {
  SIMPLE_TEST(Empty) {
    twist::CountDownLatch latch{0};
    latch.Await();  // doesn't wait
  }

  SIMPLE_TEST(CountDownOne) {
    twist::CountDownLatch latch{1};
    latch.CountDown();
    latch.Await();  // doesn't wait
  }

  SIMPLE_TEST(WaitForCountDown) {
    twist::CountDownLatch latch{2};

    auto countdown_routine = [&latch]() {
      twist::SleepMillis(500);
      latch.CountDown();
      twist::SleepMillis(500);
      latch.CountDown();
    };

    std::thread countdown_thread(countdown_routine);

    latch.Await(); // ~1 second

    countdown_thread.join();
  }
}
